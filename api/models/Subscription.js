/**
 * Subscription.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */


/*
const mongoose = require( 'mongoose' );
const { Schema } = mongoose;


const subscriptionSchema = new Schema({
  email: {
    type: 'string',
    unique: true,
    required: true,
  },
  fullName: {
    type: 'string',
    required: true,
  },
  companyName: {
    type: 'string',
    required: true,
  },
  isSubscribed: {
    type: 'boolean',
    default: true,
  },
},
{
  timestamps: true
});


module.exports = mongoose.model('Subscription', subscriptionSchema);
*/


module.exports = {

  attributes: {

    email: {
      type: 'string',
      unique: true,
      required: true,
    },
    fullName: {
      type: 'string',
      required: true,
    },
    companyName: {
      type: 'string',
      required: true,
    },
    isSubscribed: {
      type: 'boolean',
      defaultsTo: true, },
  },

  datastore: 'subscriptionDB',

};
