/**
 * SubscriptionController
 *  
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


const validator = require('validator');


function emailValidator (email) {
    // False?
    if (!validator.isEmail(email)) {
        // Send HTTP 400 - Bad Request
        return false;
    }
    // Success
    return true;
}

function fullNameValidator(fullName) {
    // False?
    if (!validator.isAlpha(fullName, ['es-ES'], { ignore: ' ' })) {
        // Send HTTP 400 - Bad Request
        return false;
    }
    // Success
    return true;
}

function companyNameValidator(companyName) {
    // False?
    if (!validator.isAlphanumeric(companyName, ['es-ES'], { ignore: ' ' })) {
        // Send HTTP 400 - Bad Request
        return false;
    }
    // Success
    return true;
}

function subscription (res, email, SubscriptionUpdate) {
    // Criteria
    let subscriptionCriteria = { email: email };
    // Query
    Subscription.findOne(subscriptionCriteria).exec(function(err, subscription) {
        // Error?
        if (err) {
            // Console Log - Error
            sails.log.debug(err);
            sails.log.debug('-------------------------------------------------------');
            // HTTP 500 - Internal Server Error
            return res.status(500).send('Unable to fetch subscriptions');
        }
        // Undefined?
        else if (!subscription) {
            // HTTP 404 - Not Found
            return res.status(404).send('This email is not recorded');
        }
        // CASE: subscribe ROUTE (TRUE)
        if (SubscriptionUpdate) {
            // isSubscribed?
            if (subscription.isSubscribed) {
                // HTTP 200 - OK
                return res.status(200).send('Email already subscribed');
            }
        }
        // CASE: unsubscribe ROUTE (FALSE)
        else {
            // isSubscribed?
            if (!subscription.isSubscribed) {
                //HTTP 200 - OK
                return res.status(200).send('Email already unsubscribed');
            }
        }
        // Criteria
        let subscriptionCriteria = { email: subscription.email };
        // Values To Set
        let subscriptionToSet = { isSubscribed: SubscriptionUpdate };
        // Query
        Subscription.updateOne(subscriptionCriteria, subscriptionToSet).exec(function(err) {
            // Error?
            if (err) {
                // Console Log - Error
                sails.log.debug(err);
                sails.log.debug('-------------------------------------------------------');
                // HTTP 500 - Internal Server Error
                return res.status(500).send('Unable to update subscription');
            }
            // CASE: subscribe ROUTE (TRUE)
            if (SubscriptionUpdate) {
                // HTTP 200 - OK
                return res.status(200).send('Email successfully subscribed');
            }
            // CASE: unsubscribe ROUTE (FALSE)
            else {
                // HTTP 200 - OK
                return res.status(200).send('Email successfully unsubscribed');
            }
        });
    });
}


module.exports = {


    add: function (req, res) {
        // Console Log - All Parameters
        sails.log.debug('Values to record:', req.allParams());
        sails.log.debug('-------------------------------------------------------');
        // Parameters
        let email = req.body.email;
        let fullName = req.body.fullName;
        let companyName = req.body.companyName;
        // Validations
        if (!emailValidator(email)) {
            // HTTP 400 - Bad Request
            return res.status(400).send('Incorrect format for email');
        }
        else if (!fullNameValidator(fullName)) {
            // HTTP 400 - Bad Request
            return res.status(400).send('Full name needs to be alphabetical');
        }
        else if (!companyNameValidator(companyName)) {
            // HTTP 400 - Bad Request
            return res.status(400).send('Company name needs to be alphanumeric');
        }
        // Criteria
        let subscriptionCriteria = { email: email };
        // Query
        Subscription.findOne(subscriptionCriteria).exec(function(err, subscription) {
            // Error?
            if (err) {
                // Console Log - Error
                sails.log.debug(err);
                sails.log.debug('-------------------------------------------------------');
                // HTTP 500 - Internal Server Error
                return res.status(500).send('Unable to fetch subscriptions');
            }
            // Undefined?
            else if (subscription) {
                // HTTP 409 - Conflic
                return res.status(409).send('This email is already recorded');
            }
            // Initial Values
            let subscriptionValues = {
                email: email,
                fullName: fullName,
                companyName: companyName,
            };
            // Query
            Subscription.create(subscriptionValues).exec(function(err) {
                // Error?
                if (err) {
                    // Console Log - Error
                    sails.log.debug(err);
                    sails.log.debug('-------------------------------------------------------');
                    // HTTP 500 - Internal Server Error
                    return res.status(500).send('Unable to record subscription');
                }
                // HTTP 200 - OK
                return res.status(200).send('Email successfully recorded');
            });
        });
    },

    subscribe: function (req, res) {
        // Console Log - Request Parameter
        sails.log.debug('Email to subscribe:', req.param('email'));
        sails.log.debug('-------------------------------------------------------');
        // Parameter Value
        let email = req.param('email');
        // Function to Subscribe
        return subscription(res, email, true);
    },

    unsubscribe: function (req, res) {
        // Console Log - Request Parameter
        sails.log.debug('Email to unsubscribe:', req.param('email'));
        sails.log.debug('-------------------------------------------------------');
        // Parameter Value
        let email = req.param('email');
        // Function to Unsubscribe
        return subscription(res, email, false);
    },

    delete: function (req, res) {
        // Console Log - Request Parameter
        sails.log.debug('Subscription to delete:', req.param('email'));
        sails.log.debug('-------------------------------------------------------');
        // Parameter Value
        let email = req.param('email');
        // Criteria
        let subscriptionCriteria = { email: email };
        // Query
        Subscription.findOne(subscriptionCriteria).exec(function(err, subscription) {
            // Error?
            if (err) {
                // Console Log - Error
                sails.log.debug(err);
                sails.log.debug('-------------------------------------------------------');
                // HTTP 500 - Internal Server Error
                return res.status(500).send('Unable to fetch subscriptions');
            }
            // Undefined?
            else if (!subscription) {
                // HTTP 404 - Not Found
                return res.status(404).send('This email is not recorded');
            }
            // Criteria
            let subscriptionCriteria = { email: email };
            // Query
            Subscription.destroy(subscriptionCriteria).exec(function(err) {
                // Error?
                if (err) {
                    // Console Log - Error
                    sails.log.debug(err);
                    sails.log.debug('-------------------------------------------------------');
                    // HTTP 500 - Internal Server Error
                    return res.status(500).send('Unable to delete subscription');
                }
                // HTTP 200 - OK
                return res.status(200).send('Subscription successfully deleted');
            });
        });
    },


};
